import * as dat from 'dat.gui';
var canvas = document.querySelector("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext("2d");
const maxRadius = 30
const minRadius = 3
var mouse = {
  x: undefined,
  y: undefined,
}
const colorArray = [
  "#FFBB22",
  "#66BBAA",
  "#00CCDD",
  "#B6AC",
  "#00CC00",
]
window.addEventListener("mousemove", function (event) {
  mouse.x = event.x;
  mouse.y = event.y;
})

var wave = {
    y: canvas.height / 2,
    amplitude: 100,
    length: 0.01,
    frequency: 0.01
}
var hsl = {
    h: 100,
    s:50,
    l: 50,
}

var bg = {
    r: 0,
    g: 0,
    b: 0,
    a: 0.05,
}
const gui = new dat.GUI();
const waveFolder = gui.addFolder("waveFolder");
waveFolder.add(wave, "y", 0, canvas.height);
waveFolder.add(wave, "amplitude", -200, 200);
waveFolder.add(wave, "length", 0, 0.5);
waveFolder.add(wave, "frequency", -0.01, 2);

const colorFolder = gui.addFolder("Line color");
colorFolder.add(hsl, "h", 0, 255);
colorFolder.add(hsl, "s", 0, 100);
colorFolder.add(hsl, "l", 0, 100);
colorFolder.open();

const bgColor = gui.addFolder("Background Color");
bgColor.add(bg, "r", 0, 255);
bgColor.add(bg, "g", 0, 255);
bgColor.add(bg, "b", 0, 255);
bgColor.add(bg, "a", 0.01, 1);
bgColor.open();

let increment = wave.frequency;

function animate() {
    requestAnimationFrame(animate);
    // c.clearRect(0,0,canvas.width,canvas.height);
    c.fillStyle = `rgba(${bg.r}, ${bg.g}, ${bg.b}, ${bg.a})`
    c.fillRect(0, 0,canvas.width,canvas.height);

    c.beginPath();
    c.moveTo(0, canvas.height / 2);
    for (let i = 0; i < canvas.width - 10; i++) {
        c.lineTo(i, wave.y + Math.sin(i * wave.length + increment) * wave.amplitude * Math.sin(increment));    
    }
    c.strokeStyle= `hsl(${Math.abs(hsl.h * Math.sin(increment))},${hsl.s}%, ${hsl.l}%)`;
    c.stroke();
    increment += wave.frequency;
}
animate();