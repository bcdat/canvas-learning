var canvas = document.querySelector("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext("2d");



// for (let i = 0; i < 100; i++) {
//   let x = Math.random() * window.innerWidth;
//   let y = Math.random() * window.innerHeight;
//   let r = Math.random() * 255;
//   let g = Math.random() * 255;
//   let b = Math.random() * 255;
//   let a = Math.random();

//   c.beginPath()
//   c.strokeStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
//   c.arc(x, y, 50, 0, Math.PI * 2, true)
//   c.stroke()
  
// }

class Circle {
  constructor(x, y, radius, dx, dy) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
  }

  drawing() {
    c.beginPath()
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true)
    c.stroke()
  }
  update() {
    if(this.x + this.radius > window.innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx
    }
    if(this.y + this.radius > window.innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy
    }
    this.x += this.dx
    this.y += this.dy
    this.drawing()
  }  

}


var circleArray = [];

for (let i = 0; i < 100; i++) {
  let x = Math.random() * (window.innerWidth - 30 * 2) + 30;
  let y = Math.random() * (window.innerHeight - 30 * 2) + 30;
  let dx = Math.random() * 5;
  let dy = Math.random() * 5;
  circleArray.push(new Circle(x, y, 30, dx, dy))
}
console.log(circleArray);

function animate () {
  requestAnimationFrame(animate)
  c.clearRect(0,0,window.innerWidth,window.innerHeight);
  circleArray.forEach( circle => circle.update())
}
animate()