var canvas = document.querySelector("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext("2d");
const maxRadius = 30
const minRadius = 3
var mouse = {
  x: undefined,
  y: undefined,
}
const colorArray = [
  "#FFBB22",
  "#66BBAA",
  "#00CCDD",
  "#B6AC",
  "#00CC00",
]
window.addEventListener("mousemove", function (event) {
  mouse.x = event.x;
  mouse.y = event.y;
})

class Circle {
  constructor(x, y, radius, dx, dy, colorArray) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.color = colorArray[parseInt(Math.random() * 4)]
  }

  drawing() {
    c.beginPath()
    c.fillStyle = this.color;
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true)
    c.stroke()
    c.fill()
  }
  update() {
    if(this.x + this.radius > window.innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx
    }
    if(this.y + this.radius > window.innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy
    }
    this.x += this.dx
    this.y += this.dy

    //Check if mouse on a circle
    if((this.x < mouse.x && this.x + this.radius> mouse.x - 30) ||
      (this.x > mouse.x && this.x - this.radius < mouse.x + 30))
      {
        if((this.y < mouse.y && this.y + this.radius > mouse.y - 50) || //Mouse on bottom circle
          (this.y > mouse.y && this.y - this.radius < mouse.y + 50))
          {
            if(this.radius < maxRadius) {
              this.radius++
            }
          } else if(this.radius > minRadius) {
            this.radius--
          }
      } else if(this.radius > minRadius) {
        this.radius--
      }
    this.drawing()
  }  

}


var circleArray = [];

for (let i = 0; i < 100; i++) {
  let x = Math.random() * (window.innerWidth - 30 * 2) + 30;
  let y = Math.random() * (window.innerHeight - 30 * 2) + 30;
  let dx = Math.random() * 2;
  let dy = Math.random() * 2;
  circleArray.push(new Circle(x, y, 30, dx, dy, colorArray))
}
console.log(circleArray);

function animate () {
  requestAnimationFrame(animate)
  c.clearRect(0,0,window.innerWidth,window.innerHeight);
  circleArray.forEach( circle => circle.update())
}
animate()