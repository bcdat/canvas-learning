var canvas = document.querySelector("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext("2d");
const maxRadius = 30
const minRadius = 3
var mouse = {
  x: undefined,
  y: undefined,
}
const colorArray = [
  "#FFBB22",
  "#66BBAA",
  "#00CCDD",
  "#B6AC",
  "#00CC00",
]
window.addEventListener("mousemove", function (event) {
  mouse.x = event.x;
  mouse.y = event.y;
})

class Circle {
  constructor(x, y, radius, dy, colorArray) {
    this.x = x;
    this.y = y;
    this.dy = dy;
    this.radius = radius;
    this.color = colorArray[parseInt(Math.random() * 4)]
  }

  drawing() {
    c.beginPath()
    c.fillStyle = this.color;
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true)
    c.stroke()
    c.fill()
  }
  update() {
    if(this.y + this.radius + this.dy> canvas.height) {
      this.dy = -this.dy * 0.8;
    } else {
      this.dy += 1;
    }
    console.log(this.y);
    this.y += this.dy;
    this.drawing()
  }  

}


// var circleArray = [];

// for (let i = 0; i < 100; i++) {
  let x = canvas.width / 2;
  let y = 100;
//   let dx = Math.random() * 2;
  let dy = 1;
//   circleArray.push()
// }
var ball = new Circle(x, y, 10, dy, colorArray)
function animate () {
  requestAnimationFrame(animate)
  c.clearRect(0,0,window.innerWidth,window.innerHeight);
  ball.update()
  // circleArray.forEach( circle => circle.update())
}
animate()