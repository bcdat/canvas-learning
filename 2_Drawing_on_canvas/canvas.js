var canvas = document.querySelector("canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext("2d");
c.fillStyle = "rgba(0, 255, 0, 0.5)"
c.fillRect(100, 100, 100, 100);
c.fillStyle = "rgba(0, 0, 255, 0.5)"
c.fillRect(200, 200, 100, 100);
c.fillStyle = "rgba(255, 0, 0, 0.5)"
c.fillRect(300, 300, 100, 100);
console.log(canvas);

// Line

c.beginPath()
c.strokeStyle = "#f453F3";
c.moveTo(200, 100);
c.lineTo(300, 200);
c.lineTo(400, 200);
c.lineTo(400, 300);
c.stroke();
// Arc/circle

for (let i = 0; i < 100; i++) {
  let x = Math.random() * window.innerWidth;
  let y = Math.random() * window.innerHeight;
  let r = Math.random() * 255;
  let g = Math.random() * 255;
  let b = Math.random() * 255;
  let a = Math.random();

  c.beginPath()
  c.strokeStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
  console.log(c.strokeStyle);
  c.arc(x, y, 50, 0, Math.PI * 2, true)
  c.stroke()
  
}